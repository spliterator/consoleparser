package service;

import java.util.stream.Stream;

public interface FileParser {

    Stream<String> parseFile(String filePath);
}
