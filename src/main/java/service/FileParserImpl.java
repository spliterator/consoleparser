package service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.regex.Pattern.UNICODE_CHARACTER_CLASS;
import static java.util.regex.Pattern.compile;

public class FileParserImpl implements FileParser {

    private static final Pattern pattern = compile("[A-Za-zА-Яа-я0-9]+", UNICODE_CHARACTER_CLASS);

    @Override
    public Stream<String> parseFile(String filePath) {
        Path path = Paths.get(filePath);
        if (Files.exists(path)) {
            try {
                return Files.readAllLines(path, StandardCharsets.UTF_8)
                        .stream()
                        .flatMap(this::parseString);
            } catch (IOException ex) {
                throw new RuntimeException("i/o error");
            }
        } else {
            throw new IllegalStateException("file not found");
        }
    }

    private Stream<? extends String> parseString(String string) {
        List<String> words = new ArrayList<>();
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            words.add(matcher.group());
        }
        return words.stream();
    }
}