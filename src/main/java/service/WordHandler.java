package service;

public interface WordHandler {

    void handleWord(String word);
}