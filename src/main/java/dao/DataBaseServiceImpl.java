package dao;

import entity.WordInfo;

import java.sql.*;
import java.util.function.Consumer;
import java.util.function.Function;

public class DataBaseServiceImpl implements DataBaseService {

    public static final String JDBC_URL = "jdbc:postgresql://localhost:5432/parser";

    @Override
    public void createTableIfNeed() {
        execWithConnection(connection -> {
            try (Statement statement = connection.createStatement()) {
                String sql = "CREATE TABLE IF NOT EXISTS WORDS (" +
                        "WORD   TEXT PRIMARY KEY    NOT NULL, " +
                        "COUNT  INT                 NOT NULL) ";
                statement.executeUpdate(sql);
            } catch (SQLException e) {
                throw new RuntimeException("sql table creation error");
            }
        });
    }

    @Override
    public WordInfo selectWordInformation(String word) {
        return execWithConnection(connection -> {
            try (Statement st = connection.createStatement()) {
                ResultSet resultSet = st.executeQuery("SELECT * FROM WORDS WHERE WORD='" + word + "'");
                if (resultSet.next()) {
                    Integer count = resultSet.getInt(2);
                    return new WordInfo(word, count);
                }
                return null;
            } catch (SQLException ex) {
                throw new RuntimeException("sql select error", ex);
            }
        });
    }

    @Override
    public void updateWordInformation(String word) {
        execWithConnection(connection -> {
            try (Statement st = connection.createStatement()) {
                st.execute("UPDATE WORDS SET COUNT=COUNT+1 WHERE WORD='" + word + "'");
            } catch (SQLException ex) {
                throw new RuntimeException("sql update error", ex);
            }
        });
    }

    @Override
    public void insertWordInformation(WordInfo wordInfo) {
        execWithConnection(connection -> {
            try (Statement statement = connection.createStatement()) {
                statement.execute("INSERT INTO WORDS VALUES ('" + wordInfo.getWord() + "',1)");
            } catch (SQLException ex) {
                throw new RuntimeException("sql insert error", ex);
            }
        });
    }

    private void execWithConnection(Consumer<Connection> consumer) {
        try (Connection connection = createConnection()) {
            consumer.accept(connection);
        } catch (SQLException ex) {
            throw new RuntimeException("Database connection error", ex);
        }
    }

    private <T> T execWithConnection(Function<Connection, T> consumer) {
        try (Connection connection = createConnection()) {
            return consumer.apply(connection);
        } catch (SQLException ex) {
            throw new RuntimeException("Database connection error", ex);
        }
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, "postgres", "postgres");
    }
}