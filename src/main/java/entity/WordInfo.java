package entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class WordInfo {

    final String word;
    final int count;

    public WordInfo incrementWordCount() {
        return new WordInfo(word, count + 1);
    }
}