public class ConsoleApplication {

    public static void main(String[] args) {
        try {
            Application.run(args);
        } catch (RuntimeException e) {
            System.err.println(e.getMessage());
        }
    }
}