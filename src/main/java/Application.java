import com.google.inject.*;
import dao.DataBaseService;
import dao.DataBaseServiceImpl;
import lombok.RequiredArgsConstructor;
import repo.WordInfoRepository;
import repo.WordInfoRepositoryImpl;
import service.FileParser;
import service.FileParserImpl;
import service.WordHandler;
import service.WordHandlerImpl;

import java.util.Arrays;

@Singleton
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class Application {

    public static void run(String[] args) throws RuntimeException{
        Injector injector = Guice.createInjector(new ApplicationModule());
        Application application = injector.getInstance(Application.class);
        application.runApp(args);
    }

    private static class ApplicationModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(WordInfoRepository.class).to(WordInfoRepositoryImpl.class);
            bind(FileParser.class).to(FileParserImpl.class);
            bind(WordHandler.class).to(WordHandlerImpl.class);
            bind(DataBaseService.class).to(DataBaseServiceImpl.class);
        }
    }

    private final FileParser fileParser;
    private final WordHandler wordHandler;

    private void runApp(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("No argument accept. Need a path to file(s)");
        } else {
            Arrays.stream(args)
                    .flatMap(fileParser::parseFile)
                    .forEach(wordHandler::handleWord);
        }
    }
}