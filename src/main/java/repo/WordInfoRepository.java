package repo;

import entity.WordInfo;

import java.util.Optional;

public interface WordInfoRepository {

    Optional<WordInfo> findByWord(String word);

    void save(WordInfo wordInfo);

    void create(WordInfo wordInfo);
}