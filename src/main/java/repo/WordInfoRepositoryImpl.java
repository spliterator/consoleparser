package repo;

import dao.DataBaseService;
import dao.DataBaseServiceImpl;
import entity.WordInfo;

import javax.inject.Inject;
import java.util.Optional;

public class WordInfoRepositoryImpl implements WordInfoRepository {

    private final DataBaseService dataBaseService;

    @Inject
    public WordInfoRepositoryImpl(DataBaseService dataBaseService) {
        this.dataBaseService = dataBaseService;
        dataBaseService.createTableIfNeed();
    }

    @Override
    public Optional<WordInfo> findByWord(String word) {
        return Optional.ofNullable(dataBaseService.selectWordInformation(word));
    }

    @Override
    public void save(WordInfo wordInfo) {
        dataBaseService.updateWordInformation(wordInfo.getWord());
    }

    @Override
    public void create(WordInfo wordInfo) {
        dataBaseService.insertWordInformation(wordInfo);
    }
}